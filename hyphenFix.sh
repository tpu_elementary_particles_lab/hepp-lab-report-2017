#!/bin/sh

SOURCE_FILES_ENCODED=`                          \
find . \( -name "*.tex" \) -type f -print0 | xxd -p`

echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
    sed -i -e "s/\([а-яА-Я]\+\)-\([а-яА-Я]\+\)/\1\\\\hyp\{\}\2/g" '{}'
#    sed -i -e "s/\([a-zA-Z]\+\)-\([а-яА-Я]\+\)/\1\\\\hyp\{\}\2/g" '{}'
#egrep '[а-яА-Я]+-[а-яА-Я]+' ./chapters/* -r
