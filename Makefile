BIBFILE=../hepp-lab-report-2017/bibtex_base_copy.bib
LATEX=/usr/bin/xelatex
BIBTEX=/usr/bin/bibtex

CHAPTERS:=$(wildcard ../hepp-lab-report-2017/chapters/*.tex)
CONVERTED_CHAPTERS=$(patsubst %.tex,%_cp1251.tex,$(CHAPTERS))

# main.pdf
all: main.pdf #$(CONVERTED_CHAPTERS)

main.pdf: ../hepp-lab-report-2017/main.tex $(CHAPTERS) $(BIBFILE)
	$(LATEX) $^
	$(BIBTEX) main
	$(LATEX) $^

recode: $(CONVERTED_CHAPTERS)

#chapters/%_cp1251.tex: chapters/%.tex
#	iconv -f utf8 -t cp1251 $^ > $@

clean:
	rm *.aux
	rm *.toc
	rm *.log
	rm *.blg
	rm *.bbl
	rm *.pdf
.PHONY: all recode

