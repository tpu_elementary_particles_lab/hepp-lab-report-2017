# Отчёт по НИР ГЗ Наука 2017 / ЛФЭЧ каф. ВММФ ТПУ
Финальный отчёт по ГЗ <<Наука>> (Contract No. 0.1526.2015, 3854) по завершению
выполнения НИР.

## Build

Out-of-source, please.

In directory where hepp-lab-report-2017 is located do:

    `$ mkdir hepp-lab-report-2017.build

     $ cd hepp-lab-report-2017.build

     $ xelatex ../hepp-lab-report-2017/main.tex

     $ bibtex main

     $ xelatex ../hepp-lab-report-2017/main.tex`

If you see in your terminal:

    '$ LaTeX Warning: Label(s) may have changed. Rerun to get cross-references right.'

do:

    '$ xelatex ../hepp-lab-report-2017/main.tex`'

again
